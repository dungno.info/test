<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Folder structure
```
.
├── charts                      -> Helm chart
│   └── lumen-backend
│       ├── charts
│       ├── environments
│       └── templates
├── src
│   ├── config
│   ├── core
│   │   ├── databases
│   │   ├── entities
│   │   ├── exceptions
│   │   ├── http
│   │   │   ├── controllers
│   │   │   └── guards
│   │   ├── repositories
│   │   │   ├── criterias
│   │   │   └── eloquents
│   │   ├── requests
│   │   ├── services
│   │   ├── tests
│   │   └── transformers
│   ├── mails
│   │   ├── adapters
│   │   ├── constants
│   │   ├── interfaces
│   │   ├── mails
│   │   └── services
│   └── users                   -> Module user
│       ├── databases
│       │   ├── factories       -> Contains factories
│       │   └── migrations      -> Contains migrations
│       ├── entities            -> Contains entities
│       ├── enums               -> Contains enums
│       ├── http
│       │   ├── controllers     -> Contains controllers
│       │   ├── guards          -> Contains guards
│       │   ├── midlewares      -> Contains midlewares
│       │   └── requests        -> Contains request
│       ├── mails               -> Contains emails
│       ├── repositories        -> Contains repositories
│       ├── resources           -> Contains information such as views, fonts, css...
│       │   └── mails           -> Contains views for email
│       ├── services            -> Contains services
│       ├── tests               -> Contains unit test, e2e test
│       └── transformers        -> Contains transformers
└── test

```
### File structure conventions
Some code examples display a file that has one or more similarly named companion files. For example, `hero.controller.ts` and `hero.service.ts`

### Single responsibility
Apply the single responsibility principle (SRP) to all components, services, and other symbols. This helps make the app cleaner, easier to read and maintain, and more testable.

## Code Rule
Nestjs is inspired by Angular, so you can use some rules from Angular.
##### Small functions
Do define small functions

Consider limiting to no more than 75 lines.

Consider limiting files to 400 lines of code.

#### Naming
##### General Naming Guidelines
Do use consistent names for all symbols.

Do follow a pattern that describes the symbol's feature then its type. The recommended pattern is `feature.type.ts`.

##### Separate file names with dots and dashes
Do use dashes to separate words in the descriptive name.

Do use dots to separate the descriptive name from the type.

Do use consistent type names for all components following a pattern that describes the component's feature then its type. A recommended pattern is `feature.type.ts`.

Do use conventional type names including `.service, .component, .pipe, .module`, and `.directive`. Invent additional type names if you must but take care not to create too many.

##### Symbols and file names

Do use consistent names for all assets named after what they represent.

Do use upper camel case for class names.

Do match the name of the symbol to the name of the file.

Do append the symbol name with the conventional suffix (such as Component, Directive, Module, Pipe, or Service) for a thing of that type.

Do give the filename the conventional suffix (such as `.component.ts`, `.directive.ts`, `.module.ts`, `.pipe.ts`, or `.service.ts`) for a file of that type.

##### Unit test
Do name test specification files the same as the component they test.

Do name test specification files with a suffix of `.spec.ts`

##### E2E test
Do name end-to-end test specification files after the feature they test with a suffix of `.e2e-spec.ts`

## Requirements

>All APIs must have an e2e test.
>
>Complex functions must have unit tests.
>
## Docker
```$xslt
docker-compose up -d
```

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Make migration

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
