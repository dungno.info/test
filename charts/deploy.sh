apt-get install gettext-base
echo $DEV2_KUBE_CONFIG > tmp.file
mkdir /root/.kube/
base64 -di tmp.file > /root/.kube/config
helm repo add bitnami https://charts.bitnami.com/bitnami/
oras login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
oras pull $CI_REGISTRY_IMAGE:lumen-backend-1.0.0.tgz
tar -zxf lumen-backend-1.0.0.tgz
envsubst "$MYVARS" < ./charts/lumen-backend/environments/$K_VALUE_FILE > values.yaml
helm upgrade $K_NAMESPACE --namespace $K_NAMESPACE -f ./values.yaml --install ./lumen-backend