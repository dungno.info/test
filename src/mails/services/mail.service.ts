import { Inject, Injectable } from '@nestjs/common';
import { createTransport } from 'nodemailer';
import * as Mail from 'nodemailer/lib/mailer';
import BaseMail from '../mails/base.mail';
import { MAILER_OPTIONS } from '../constants/mailer.constant';
import { MailerOptions } from '../interfaces/mailer-options.interface';
import { MailContentService } from './mail-content.service';

@Injectable()
export class MailService {
    private transport: Mail;

    constructor(
        @Inject(MAILER_OPTIONS) private readonly mailerOptions: MailerOptions,
        private mailContentService: MailContentService
    ) {
        this.transport = createTransport(mailerOptions.transport);
    }

    public async send(mail: BaseMail) {
        let content = await this.mailContentService.getContent(mail);
        await mail.afterRender();
        let options = {
            from: mail.from,
            to: mail.to,
            subject: mail.subject,
            html: content,
            cc: mail.cc,
            ...mail.options
        };
        await this.transport.sendMail(options);
    }
}
