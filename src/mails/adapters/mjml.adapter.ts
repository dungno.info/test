import { BaseViewAdapter } from './base-view.adapter';
import BaseMail from '../mails/base.mail';
import mjmlToHtml = require('mjml');

export class MjmlAdapter extends BaseViewAdapter {
    public render(mail: BaseMail) {
        return mjmlToHtml(this.getTemplateContent(mail)).html;
    }
}
