import * as Handlebars from 'handlebars';
import BaseMail from '../mails/base.mail';
import { BaseViewAdapter } from './base-view.adapter';

export class HbsAdapter extends BaseViewAdapter {
    async render(mail: BaseMail): Promise<string> {
        let template = Handlebars.compile(this.getTemplateContent(mail), {});
        return template(await mail.data());
    }
}
