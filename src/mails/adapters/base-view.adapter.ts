import BaseMail from '../mails/base.mail';
import { readFileSync } from 'fs';

export abstract class BaseViewAdapter {
    public abstract render(mail: BaseMail): Promise<string> | string;

    protected getTemplateContent(mail: BaseMail) {
        if (mail.content) {
            return mail.content;
        }
        return readFileSync(mail.template).toString();
    }
}
