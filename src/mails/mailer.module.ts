import { DynamicModule, Global, Module, ValueProvider } from '@nestjs/common';
import { MailContentService } from './services/mail-content.service';
import { MailService } from './services/mail.service';
import { MailerOptions } from './interfaces/mailer-options.interface';
import { MAILER_OPTIONS } from './constants/mailer.constant';

@Global()
@Module({})
export class MailerModule {
    public static forRoot(options: MailerOptions): DynamicModule {
        const MailerOptionsProvider: ValueProvider<MailerOptions> = {
            provide: MAILER_OPTIONS,
            useValue: options
        };

        return {
            module: MailerModule,
            providers: [MailerOptionsProvider, MailService, MailContentService],
            exports: [MailService]
        };
    }
}
