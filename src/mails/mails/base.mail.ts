import { Attachment } from 'nodemailer/lib/mailer';

export default abstract class BaseMail {
    public content: string = '';
    public attachments = [];

    abstract get subject(): string;

    abstract get to(): string;

    abstract get from(): string;

    public get cc() {
        return [];
    }

    abstract get template(): string;

    public data() {
        return {};
    }

    public get options() {
        return {};
    }

    public listAttachments(): Attachment[] {
        return this.attachments;
    }

    public addAttachment(attachment: Attachment) {
        this.attachments.push(attachment);
        return this;
    }

    public afterRender() {}
}
