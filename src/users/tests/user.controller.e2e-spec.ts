import { TestHelper } from '../../core/tests/test.helper';

describe('UserController (e2e)', () => {
    let testHelper = new TestHelper();

    beforeEach(async () => {
        await testHelper.initialize();
    });

    it('/users 1', () => {
        return testHelper.get('/users').authenticate({}).expect(200);
    });
});
