import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';
import ImageTransformer from '../../core/transformers/image.transformer';
import { BaseEntity } from '../../core/entities/base.entity';
import EmailTransformer from '../../core/transformers/email.transformer';
import { UserRoleEnum } from '../enums/user-role.enum';
import { Exclude } from 'class-transformer';

@Entity('users')
export class UserEntity extends BaseEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    name: string;

    @Exclude()
    @Column()
    password: string;

    @Index()
    @Column({ transformer: new EmailTransformer() })
    email: string;

    @Column({ nullable: true, type: 'varchar' })
    phone_number: string;

    @Column({ nullable: true, type: 'simple-json', transformer: new ImageTransformer() })
    avatar: any;

    @Column({ type: 'integer' })
    role: UserRoleEnum;

    @Column({ type: 'varchar', default: 'en' })
    language: string;
}
