import { Body, Controller, Post } from '@nestjs/common';
import { UserService } from '../../services/user.service';
import { BaseController } from '../../../core/http/controllers/base.controller';
import { LoginRequest } from '../requests/login.request';

@Controller('auth')
export class AuthController extends BaseController {
    constructor(private readonly userService: UserService) {
        super();
    }

    @Post('login')
    async login(@Body() request: LoginRequest) {
        console.log(request);
    }
}
