import { Controller, Get } from '@nestjs/common';
import { UserService } from '../../services/user.service';
import { BaseController } from '../../../core/http/controllers/base.controller';

@Controller('users')
export class UserController extends BaseController {
    constructor(private readonly userService: UserService) {
        super();
    }

    @Get()
    async test() {
        return { key: await this.userService.test() };
    }
}
