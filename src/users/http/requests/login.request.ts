import { IsString, IsOptional, IsNotEmpty, MaxLength, MinLength, ValidateNested } from 'class-validator';
import { TestRequest } from './test.request';
import { Type } from 'class-transformer';

export class LoginRequest {
    @IsNotEmpty()
    email: string;

    @IsString()
    @MinLength(15)
    password: string;

    @ValidateNested()
    @Type(() => TestRequest)
    test: TestRequest;
}
