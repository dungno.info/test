import { IsString, IsOptional, IsNotEmpty, MaxLength, MinLength } from 'class-validator';

export class TestRequest {
    @IsNotEmpty()
    email: string;

    @IsString()
    @MinLength(15)
    password: string;
}
