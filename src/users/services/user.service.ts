import { ForbiddenException, Injectable, UnauthorizedException, UnprocessableEntityException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from '../repositories/user.repository';

@Injectable()
export class UserService {
    public constructor(
        @InjectRepository(UserRepository)
        private readonly userRepo: UserRepository
    ) {}

    public test() {
        return this.userRepo.count();
    }

    public findUserById(id) {
        return this.userRepo.findById(id);
    }
}
