import { Module } from '@nestjs/common';
import { UserService } from './services/user.service';
import { UserController } from './http/controllers/user.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './entities/user.entity';
import { UserRepository } from './repositories/user.repository';
import { MailerModule } from '../mails/mailer.module';
import { HbsAdapter } from '../mails/adapters/hbs.adapter';
import { MjmlAdapter } from '../mails/adapters/mjml.adapter';
import { AuthController } from './http/controllers/auth.controller';

@Module({
    providers: [UserService],
    controllers: [UserController, AuthController],
    imports: [TypeOrmModule.forFeature([UserEntity, UserRepository])]
})
export class UserModule {}
