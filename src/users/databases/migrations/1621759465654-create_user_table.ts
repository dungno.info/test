import { QueryRunner } from 'typeorm';
import { BaseMigration } from '../../../core/databases/base.migration';

export class CreateUserTable1621759465654 extends BaseMigration {
    async rollback(queryRunner: QueryRunner) {}

    async run(queryRunner: QueryRunner) {
        await this.create('users', (table) => {
            table.uuid();
            table.string('email').index();
            table.string('name').nullable();
            table.string('password');
            table.integer('role');
        });
    }
}
