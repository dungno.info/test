import { define } from 'typeorm-seeding';
import * as Faker from 'faker';
import * as bcrypt from 'bcrypt';
import { UserEntity } from '../../entities/user.entity';
import { UserRoleEnum } from '../../enums/user-role.enum';

define(UserEntity, (faker: typeof Faker, settings: { roles: string[] }) => {
    const gender = faker.random.number(1);
    const firstName = faker.name.firstName(gender);
    const lastName = faker.name.lastName(gender);
    const email = faker.internet.email(firstName, lastName);
    const user = new UserEntity();
    user.email = faker.internet.email(firstName, lastName);
    user.name = firstName + ' ' + lastName;
    user.role = UserRoleEnum.IS_USER;
    user.password = bcrypt.hashSync('123456', 10);
    user.avatar = {
        origin: faker.image.avatar(),
        thumbnail: faker.image.avatar()
    };
    return user;
});
