import BaseMail from '../../mails/mails/base.mail';
import * as path from 'path';

export class WelcomeMail extends BaseMail {
    get subject(): string {
        return '';
    }

    get template(): string {
        return path.join(__dirname, '/mails/tests.hbs');
    }

    get to(): string {
        return '30e2da2ab8-d4771b@inbox.mailtrap.io';
    }

    get from(): string {
        return '';
    }
}
