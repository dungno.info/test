import { EntityRepository } from 'typeorm';
import { UserEntity } from '../entities/user.entity';
import { BaseRepository } from '../../core/repositories/eloquents/base.repository';

@EntityRepository(UserEntity)
export class UserRepository extends BaseRepository<UserEntity> {}
