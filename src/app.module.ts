import { CacheModule, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './users/user.module';
import { MailerModule } from './mails/mailer.module';
import { databaseConfig } from './config/database.config';
import * as redisStore from 'cache-manager-redis-store';
import { env } from './config/env.config';
import { mailConfig } from './config/mail.config';

@Module({
    imports: [
        databaseConfig,
        UserModule,
        MailerModule,
        CacheModule.register({
            store: redisStore,
            host: env.REDIS.HOST,
            port: env.REDIS.PORT
        }),
        mailConfig
    ],
    controllers: [AppController],
    providers: [AppService]
})
export class AppModule {}
