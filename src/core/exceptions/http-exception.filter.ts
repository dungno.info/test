import { ArgumentsHost, Catch, ExceptionFilter, HttpStatus } from '@nestjs/common';
import { Response } from 'express';
import { isString } from '@nestjs/common/utils/shared.utils';

@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
    private response: Response;

    catch(exception: any, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        this.response = ctx.getResponse<Response>();
        try {
            if (exception.name === 'JsonWebTokenError') {
                this.responseError(HttpStatus.UNAUTHORIZED, this.getMessage(exception));
            } else if (exception.name === 'EntityNotFound') {
                const messageRegex = /"[a-zA-Z]+"/.exec(exception.message);
                let message = exception.message;
                if (messageRegex) {
                    message = messageRegex[0].replace('"', '').replace('"', '');
                }
                this.responseError(HttpStatus.NOT_FOUND, this.convertNotFoundMessage(message));
            } else if (exception.getStatus) {
                const status = exception.getStatus();
                switch (status) {
                    case HttpStatus.BAD_REQUEST:
                        const errors = {};
                        for (const message of exception.response.message) {
                            errors[message.property] = null;
                            if (message.constraints) {
                                Object.keys(message.constraints).forEach((key) => {
                                    errors[message.property] = message.constraints[key];
                                });
                            } else {
                                errors[message.property] = `Field ${message.property} is malformed.`;
                            }
                        }
                        this.responseError(HttpStatus.UNPROCESSABLE_ENTITY, null, errors);
                        break;
                    case HttpStatus.UNPROCESSABLE_ENTITY:
                        this.responseError(HttpStatus.UNPROCESSABLE_ENTITY, null, exception.response);
                        break;
                    case HttpStatus.NOT_FOUND:
                        this.responseError(HttpStatus.NOT_FOUND, exception.message);
                        break;
                    case HttpStatus.UNAUTHORIZED:
                    default:
                        this.responseError(status, this.getMessage(exception));
                        break;
                }
            } else {
                console.log(exception);
                this.responseError(HttpStatus.INTERNAL_SERVER_ERROR, 'An error occurred during processing.');
            }
        } catch (e) {
            console.log(exception);
            this.responseError(HttpStatus.INTERNAL_SERVER_ERROR, 'An error occurred during processing.');
        }
    }

    private responseError(code, message, errors = null) {
        this.response.status(code).json({
            message: message,
            errors: errors
        });
    }

    private getMessage(exception) {
        if (isString(exception.message)) {
            return exception.message;
        } else {
            return exception.message.message;
        }
    }

    private convertNotFoundMessage(message) {
        switch (message) {
            case 'PatientEntity':
                return 'The patient is not found.';
            case 'UserEntity':
                return 'The user is not found.';
            case 'GroupEntity':
                return 'The group is not found.';
            case 'AlarmEntity':
                return 'The alarm setting is not found.';
            default:
                return message;
        }
    }
}
