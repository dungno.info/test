import { HttpException, HttpStatus, ValidationError } from '@nestjs/common';

export class ValidateException extends HttpException {
    constructor(errors: ValidationError[]) {
        console.log(errors);
        let newErrors = {};
        errors.forEach((error) => {
            newErrors[error.property] = Object.values(error.constraints);
        });
        super(newErrors, HttpStatus.UNPROCESSABLE_ENTITY);
    }
}
