import { ValueTransformer } from 'typeorm';

export default class TimestampTransformer implements ValueTransformer {
    to(value) {
        return value;
    }

    from(value) {
        return Math.round(+new Date(value) / 1000);
    }
}
