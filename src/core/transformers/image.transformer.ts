import { ValueTransformer } from 'typeorm';
import { env } from '../../config/env.config';

export default class ImageTransformer implements ValueTransformer {
    to(value) {
        return value;
    }

    from(value) {
        if (value) {
            if (value.origin.startsWith('http')) {
                return value;
            }
            return {
                origin: env.APP_URL + value.origin,
                thumbnail: env.APP_URL + value.thumbnail
            };
        }
        return {
            origin: env.APP_URL + '/assets/images/default-placeholder.jpg',
            thumbnail: env.APP_URL + '/assets/images/default-placeholder.jpg'
        };
    }
}
