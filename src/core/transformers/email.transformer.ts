import { ValueTransformer } from 'typeorm';

export default class EmailTransformer implements ValueTransformer {
    to(value: string) {
        if (!value) {
            return value;
        }
        return value.toLowerCase();
    }

    from(value) {
        return value;
    }
}
