import * as request from 'supertest';
import superagent from 'superagent';

declare module 'supertest' {
    interface Test extends superagent.SuperAgentRequest {
        authenticate(user: any): this;
    }
}

const Test = (request as any).Test;
Test.prototype.authenticate = function (user) {
    return this;
};
