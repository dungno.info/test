import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../app.module';
import { INestApplication } from '@nestjs/common';
import { CallbackHandler } from 'supertest';
import * as request from 'supertest';
import './suppertest.helper';

export class TestHelper {
    public app: INestApplication;
    public httpService: any;

    async initialize() {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule]
        }).compile();

        this.app = moduleFixture.createNestApplication();
        await this.app.init();
        this.httpService = this.app.getHttpServer();
    }

    get(url: string, callback?: CallbackHandler) {
        return request(this.httpService).get(url, callback);
    }
}
