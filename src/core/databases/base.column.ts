import { TableColumn } from 'typeorm';

export class BaseColumn {
    public isIndex = false;

    public constructor(public tableColumn: TableColumn) {}

    public length(length: number) {
        this.tableColumn.length = length as any;
        return this;
    }

    public nullable() {
        this.tableColumn.isNullable = true;
        return this;
    }

    public index() {
        this.isIndex = true;
        return this;
    }
}
