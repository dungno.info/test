import { TableColumnOptions } from 'typeorm/schema-builder/options/TableColumnOptions';
import { TableColumn } from 'typeorm';
import { BaseColumn } from './base.column';

export class BaseTable {
    private columns: BaseColumn[] = [];
    public columnToDeletes: string[] = [];

    public getNewColumns() {
        return this.columns.map((column) => column.tableColumn);
    }

    public dropColumn(column) {
        this.columnToDeletes.push(column);
    }

    public getIndexColumns() {
        return this.columns.filter((column) => column.isIndex).map((column) => column.tableColumn);
    }

    private getColumnValue(intOptions: TableColumnOptions, options: TableColumnOptions) {
        return new BaseColumn(
            new TableColumn({
                ...intOptions,
                options
            } as any)
        );
    }

    public string(name: string, length: number = 255, options: TableColumnOptions = null) {
        let column = this.getColumnValue(
            {
                name: name,
                type: 'varchar',
                length: length as any
            },
            options
        );

        this.columns.push(column);
        return column;
    }

    public uuid(name: string = 'id', options: TableColumnOptions = null) {
        let column = this.getColumnValue(
            {
                name: name,
                type: 'uuid',
                isPrimary: true
            },
            options
        );
        this.columns.push(column);
        return column;
    }

    public integer(name: string = 'id', options: TableColumnOptions = null) {
        let column = this.getColumnValue(
            {
                name: name,
                type: 'integer'
            },
            options
        );
        this.columns.push(column);
        return column;
    }
}
