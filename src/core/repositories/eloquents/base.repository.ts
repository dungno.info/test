import { FindConditions, FindManyOptions, ObjectLiteral, Repository as AbstractRepository } from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';

export abstract class BaseRepository<Entity extends ObjectLiteral> extends AbstractRepository<Entity> {
    async createOne(entity: QueryDeepPartialEntity<Entity>) {
        let data = await super.insert(entity);
        return await this.findById(data.identifiers[0].id);
    }

    async findById(id: number) {
        return await super.findOneOrFail(id);
    }

    async pagination(
        options: FindManyOptions<Entity> | FindConditions<Entity> | any,
        page: number,
        limit: number = 10
    ) {
        if (!page) {
            page = 1;
        }
        let count = await super.count(options);
        let query: any = {};
        if (!options.where) {
            query.where = options;
        } else {
            query = options;
        }
        let items: any[] | Entity = await super.find({
            ...query,
            take: limit,
            skip: (page - 1) * limit
        });

        return {
            items: items,
            total: count,
            last_page: Math.ceil(count / limit),
            per_page: limit,
            current_page: page
        };
    }

    async firstOrCreate(options: QueryDeepPartialEntity<Entity>) {
        let item = await super.findOne(options);
        if (!item) {
            return await this.createOne(options);
        }
        return item;
    }
}
