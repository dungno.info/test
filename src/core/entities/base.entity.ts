import { BaseEntity as AbstractEntity, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import TimestampTransformer from '../transformers/timestamp.transformer';

export abstract class BaseEntity extends AbstractEntity {
    @CreateDateColumn({ type: 'timestamp with time zone', transformer: new TimestampTransformer() })
    created_at: Date;

    @UpdateDateColumn({
        type: 'timestamp with time zone',
        transformer: new TimestampTransformer()
    })
    updated_at: Date;
}
