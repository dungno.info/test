import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { env } from './config/env.config';
import { UnprocessableEntityException, ValidationPipe } from '@nestjs/common';
import { HttpExceptionFilter } from './core/exceptions/http-exception.filter';
import { ValidateException } from './core/exceptions/validate.exception';

async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    app.useGlobalPipes(
        new ValidationPipe({
            whitelist: true,
            exceptionFactory: (errors) => new ValidateException(errors)
        })
    );
    app.useGlobalFilters(new HttpExceptionFilter());
    await app.listen(env.APP_PORT);
}

bootstrap();
