import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { MailService } from './mails/services/mail.service';
import { WelcomeMail } from './users/mails/welcome.mail';

@Controller()
export class AppController {
    constructor(private readonly appService: AppService, private mailService: MailService) {}

    @Get()
    getHello(): string {
        return this.appService.getHello();
    }
}
