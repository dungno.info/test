import * as dotenv from 'dotenv';

dotenv.config();

export const env = {
    APP_PORT: process.env.APP_PORT,
    APP_ENV: process.env.APP_ENV,
    APP_KEY: process.env.APP_KEY,
    APP_URL: process.env.APP_URL,
    ROOT_PATH: process.cwd(),
    DATABASE: {
        CONNECT: process.env.DATABASE_CONNECT as any,
        HOST: process.env.DATABASE_HOST,
        PORT: Number(process.env.DATABASE_PORT),
        USER: process.env.DATABASE_USER,
        PASSWORD: process.env.DATABASE_PASSWORD,
        NAME: process.env.DATABASE_NAME
    },
    REDIS: {
        HOST: process.env.REDIS_HOST,
        PORT: Number(process.env.REDIS_PORT || 6379),
        USER: process.env.REDIS_USER,
        PASS: process.env.REDIS_PASS
    },
    SMTP: {
        HOST: process.env.SMTP_HOST,
        PORT: Number(process.env.SMTP_PORT),
        USERNAME: process.env.SMTP_USER,
        PASSWORD: process.env.SMTP_PASS,
        FROM: process.env.MAIL_SEND_FROM
    }
};
