import { MailerModule } from '../mails/mailer.module';
import { HbsAdapter } from '../mails/adapters/hbs.adapter';
import { MjmlAdapter } from '../mails/adapters/mjml.adapter';
import { env } from './env.config';

export const mailConfig = MailerModule.forRoot({
    renders: {
        adapters: [new HbsAdapter(), new MjmlAdapter()]
    },
    transport: {
        host: env.SMTP.HOST,
        port: env.SMTP.PORT,
        auth: {
            user: env.SMTP.USERNAME,
            pass: env.SMTP.PASSWORD
        }
    }
});
