FROM node:lts-alpine As development
WORKDIR /usr/src/app
ADD package*.json ./
RUN npm install
ADD . .
RUN npm run build

FROM node:lts-alpine as production
WORKDIR /usr/src/app
ADD pm2.json .
ADD package*.json ./
RUN npm install pm2 -g
COPY --from=0 /usr/src/app/node_modules/ ./node_modules/
RUN npm install
COPY --from=development /usr/src/app/dist ./dist
EXPOSE 3000
CMD [ "pm2-runtime", "start", "pm2.json" ]
