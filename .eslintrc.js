module.exports = {
    parser: '@typescript-eslint/parser',
    parserOptions: {
        project: 'tsconfig.json',
        sourceType: 'module'
    },
    plugins: ['@typescript-eslint/eslint-plugin'],
    extends: [
        'plugin:prettier/recommended',
        'plugin:@typescript-eslint/recommended'
    ],
    root: true,
    env: {
        node: true,
        jest: true
    },
    ignorePatterns: ['.eslintrc.js'],
    rules: {
        '@typescript-eslint/interface-name-prefix': 'off',
        '@typescript-eslint/explicit-function-return-type': 'off',
        '@typescript-eslint/explicit-module-boundary-types': 'off',
        '@typescript-eslint/no-explicit-any': 'off',
        '@typescript-eslint/no-inferrable-types': 'off',
        'indent': 'off',
        '@typescript-eslint/indent': ['error', 4],
        'prettier/prettier': [
            'error',
            {
                'useTabs': false,
                'tabWidth': 4,
                'printWidth': 120,
                'singleQuote': true,
                'trailingComma': 'none'
            }
        ],
        'prefer-const': "off"
    }
};
